# Prisma Cloud IaC MVP Example

## Description

A minimum viable Terraform project for IaC scanning via the following integrations.

* AWS DevOps
* Azure DevOps
* CircleCI
* GitHub
* IntelliJ IDEA
* Visual Studio Code

To Do:

* BitBucket
* GitLab
* Jenkins

## Usage

Download, or fork and download, and remove/replace all `tags` in `.prismaCloud/config.yml`.

## Reference

https://docs.paloaltonetworks.com/prisma/prisma-cloud/prisma-cloud-admin/prisma-cloud-devops-security

TJK
